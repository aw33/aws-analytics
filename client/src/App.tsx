import React, { useState } from "react";
import Amplify, { Analytics, AWSKinesisFirehoseProvider } from "aws-amplify";
import config from "./aws-exports";
import "./App.css";

Amplify.configure({
  Auth: {
    identityPoolId: config.aws_cognito_identity_pool_id,
    region: config.aws_project_region,
  },
  Analytics: {
    AWSKinesisFirehose: {
      region: config.aws_project_region,
    },
  },
});

Analytics.addPluggable(new AWSKinesisFirehoseProvider());

interface ILang {
  name: string;
  votes: number;
}

const App: React.FC<{}> = () => {
  const [languages, setLanguages] = useState<ILang[]>([
    { name: "Php", votes: 0 },
    { name: "Python", votes: 0 },
    { name: "Go", votes: 0 },
    { name: "Java", votes: 0 },
  ]);

  const vote = (lang: ILang) => {
    setLanguages((prev) => {
      return prev.map((item) =>
        item === lang
          ? {
              ...item,
              votes: item.votes + 1,
            }
          : item
      );
    });

    const now = new Date();

    let data = {
      id: now.getTime(),
      language: lang.name,
    };

    Analytics.record(
      {
        data: data,
        streamName: config.aws_firehose_name,
      },
      "AWSKinesisFirehose"
    );
  };

  return (
    <>
      <h1>Vote your language!</h1>
      <div className="languages">
        {languages.map((lang) => (
          <div key={lang.name} className="language">
            <div className="voteCount">{lang.votes}</div>
            <div className="languageName">{lang.name}</div>
            <button onClick={() => vote(lang)}>Click Here</button>
          </div>
        ))}
      </div>
    </>
  );
};

export default App;
