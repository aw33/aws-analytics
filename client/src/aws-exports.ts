const config = {
  aws_project_region: "eu-west-1",
  aws_cognito_identity_pool_id:
    "eu-west-1:c6b007d7-73c4-408c-aaba-f107d5e91448",
  aws_firehose_name: "voting-app-firehose",
};

export default config;
